# MIPS Compiler

This project is a MIPS Compiler written in C. MIPS is an assembly language
that we use in labs as a sample assembly language. We also have projects
written purely in MIPS.

This programme takes in a text document with written MIPS code, seen in the repo
as "prog.txt".

The programme first takes the MIPS instructions and turns them into 32-bit
machine code. Then the C code interprets these instructions bit by bit, in order
to execute them. There are only a limited number of MIPS instructions that work,
which are:

ADD
ADDI 
ANDI
BEQ
BNE
SLL
SLR