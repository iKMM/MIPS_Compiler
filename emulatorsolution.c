#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define XSTR(x) STR(x) 
#define STR(x) #x 

#define MAX_PROG_LEN 250
#define MAX_LINE_LEN 50
#define MAX_OPCODE   8 
#define MAX_REGISTER 32 
#define MAX_ARG_LEN  20 

#define ADDR_TEXT    0x00400000 
#define TEXT_POS(a)  ((a==ADDR_TEXT)?(0):(a - ADDR_TEXT)/4)

const char *register_str[] = {	"$zero", 
				"$at",
				"$v0", "$v1",
				"$a0", "$a1", "$a2", "$a3",
				"$t0", "$t1", "$t2", "$t3", "$t4", "$t5", "$t6", "$t7",
				"$s0", "$s1", "$s2", "$s3", "$s4", "$s5", "$s6", "$s7",
				"$t8", "$t9",
				"$k0", "$k1",
				"$gp",
				"$sp",
				"$fp",
				"$ra"
				};

unsigned int registers[MAX_REGISTER] = {0};
unsigned int pc = 0;
unsigned int text[MAX_PROG_LEN] = {0};

int lastInstruction = 0;

typedef int (*opcode_function)(unsigned int, unsigned int*, char*, char*, char*, char*);

typedef int (*funct_functions)(unsigned int, unsigned int, unsigned int, unsigned int); //typedefinition for calling the R type instructions which have Funct bits.

typedef int (*opcode_functions)(unsigned int, unsigned int, int); //typedefinition for calling I Type instructions - only Opcodes

char prog[MAX_PROG_LEN][MAX_LINE_LEN];
int prog_len=0;


int print_registers(){
	int i;
	printf("registers:\n"); 
	for(i=0;i<MAX_REGISTER;i++){
		printf(" %d: %d\n", i, registers[i]); 
	}
	printf(" Program Counter: 0x%08x\n", pc);
	return(0);
}

int add_imi(unsigned int *bytecode, int imi){
	if (imi<-32768 || imi>32767) return (-1);
	*bytecode|= (0xFFFF & imi);
	return(0);
}

int add_sht(unsigned int *bytecode, int sht){
	if (sht<0 || sht>31) return(-1);
	*bytecode|= (0x1F & sht) << 6;
	return(0);
}

int add_reg(unsigned int *bytecode, char *reg, int pos){
	int i;
	for(i=0;i<MAX_REGISTER;i++){
		if(!strcmp(reg,register_str[i])){
			*bytecode |= (i << pos); 
			return(0);
		}
	}
	return(-1);
} 

int add_lbl(unsigned int offset, unsigned int *bytecode, char *label){
	char l[MAX_ARG_LEN+1];	
	int j=0;
	while(j<prog_len){
		memset(l,0,MAX_ARG_LEN+1);
		sscanf(&prog[j][0],"%" XSTR(MAX_ARG_LEN) "[^:]:", l);
		if (label!=NULL && !strcmp(l, label)) return(add_imi( bytecode, j-(offset+1)) );
		j++;
	}
	return (-1);
}

int opcode_nop(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0;
	return (0);
}

int opcode_add(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0x20; 				// op,shamt,funct
	if (add_reg(bytecode,arg1,11)<0) return (-1); 	// destination register
	if (add_reg(bytecode,arg2,21)<0) return (-1);	// source1 register
	if (add_reg(bytecode,arg3,16)<0) return (-1);	// source2 register
	return (0);
}

int opcode_addi(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0x20000000; 				// op
	if (add_reg(bytecode,arg1,16)<0) return (-1);	// destination register
	if (add_reg(bytecode,arg2,21)<0) return (-1);	// source1 register
	if (add_imi(bytecode,atoi(arg3))) return (-1);	// constant
	return (0);
}

int opcode_andi(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0x30000000; 				// op
	if (add_reg(bytecode,arg1,16)<0) return (-1); 	// destination register
	if (add_reg(bytecode,arg2,21)<0) return (-1);	// source1 register
	if (add_imi(bytecode,atoi(arg3))) return (-1);	// constant 
	return (0);
}

int opcode_beq(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0x10000000; 				// op
	if (add_reg(bytecode,arg1,21)<0) return (-1);	// register1
	if (add_reg(bytecode,arg2,16)<0) return (-1);	// register2
	if (add_lbl(offset,bytecode,arg3)) return (-1); // jump 
	return (0);
}

int opcode_bne(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0x14000000; 				// op
	if (add_reg(bytecode,arg1,21)<0) return (-1); 	// register1
	if (add_reg(bytecode,arg2,16)<0) return (-1);	// register2
	if (add_lbl(offset,bytecode,arg3)) return (-1); // jump 
	return (0);
}

int opcode_srl(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0x2; 					// op
	if (add_reg(bytecode,arg1,11)<0) return (-1);   // destination register
	if (add_reg(bytecode,arg2,16)<0) return (-1);   // source1 register
	if (add_sht(bytecode,atoi(arg3))<0) return (-1);// shift 
	return(0);
}

int opcode_sll(unsigned int offset, unsigned int *bytecode, char *opcode, char *arg1, char *arg2, char *arg3 ){
	*bytecode=0; 					// op
	if (add_reg(bytecode,arg1,11)<0) return (-1);	// destination register
	if (add_reg(bytecode,arg2,16)<0) return (-1); 	// source1 register
	if (add_sht(bytecode,atoi(arg3))<0) return (-1);// shift 
	return(0);
}

const char *opcode_str[] = {"nop", "add", "addi", "andi", "beq", "bne", "srl", "sll"};
opcode_function opcode_func[] = {&opcode_nop, &opcode_add, &opcode_addi, &opcode_andi, &opcode_beq, &opcode_bne, &opcode_srl, &opcode_sll};

int make_bytecode(){
	unsigned int bytecode;
       	int j=0;
       	int i=0;

	char label[MAX_ARG_LEN+1];
	char opcode[MAX_ARG_LEN+1];
        char arg1[MAX_ARG_LEN+1];
        char arg2[MAX_ARG_LEN+1];
        char arg3[MAX_ARG_LEN+1];

       	printf("ASSEMBLING PROGRAM ...\n");
	while(j<prog_len){
		memset(label,0,sizeof(label)); 
		memset(opcode,0,sizeof(opcode)); 
		memset(arg1,0,sizeof(arg1)); 
		memset(arg2,0,sizeof(arg2)); 
		memset(arg3,0,sizeof(arg3));	

		bytecode=0;

		if (strchr(&prog[j][0], ':')){ //check if the line contains a label
			if (sscanf(&prog[j][0],"%" XSTR(MAX_ARG_LEN) "[^:]: %" XSTR(MAX_ARG_LEN) "s %" XSTR(MAX_ARG_LEN) "s %" XSTR(MAX_ARG_LEN) 
				"s %" XSTR(MAX_ARG_LEN) "s", label, opcode, arg1, arg2, arg3) < 2){ //parse the line with label
					printf("ERROR: parsing line %d\n", j);
					return(-1);
			}
		}
		else {
			if (sscanf(&prog[j][0],"%" XSTR(MAX_ARG_LEN) "s %" XSTR(MAX_ARG_LEN) "s %" XSTR(MAX_ARG_LEN) 
				"s %" XSTR(MAX_ARG_LEN) "s", opcode, arg1, arg2, arg3) < 1){ //parse the line without label
					printf("ERROR: parsing line %d\n", j);
					return(-1);
			}
		}
       		
		for (i=0; i<MAX_OPCODE; i++){
                	if (!strcmp(opcode, opcode_str[i]) && ((*opcode_func[i])!=NULL)) {
                               	if ((*opcode_func[i])(j,&bytecode,opcode,arg1,arg2,arg3)<0) {
					printf("ERROR: line %d opcode error (assembly: %s %s %s %s)\n", j, opcode, arg1, arg2, arg3);
					return(-1);
				}
				else {
					printf("0x%08x 0x%08x\n",ADDR_TEXT + 4*j, bytecode);
					text[j] = bytecode;
                               		break;
				}
                	}
			if(i==(MAX_OPCODE-1)) {
				printf("ERROR: line %d unknown opcode\n", j);
				return(-1);
			}
        	}
		j++;

		lastInstruction = prog_len *4;
       	}
       	printf("... DONE!\n");
       	return(0);
}

/**
	Function to test for negative numbers.
	Takes in the immediate and checks if the first binary digit is a 1. If so then performs bitwise logical OR
	so it will be stored as negative.
**/

int negativeNumber(int imm)
{
	if(imm & 0x00008000)
	{
		imm |= 0xFFFF0000;
		return imm;
	}
	
	return imm;
}

/**
	The add execution. Checks if both numbers already stored in register
	are negative. Then adds the registers together and stores them in rd.
**/
int exec_add(unsigned int rs, unsigned int rt, unsigned int rd, unsigned int shamt)
{
	registers[rs] = negativeNumber(registers[rs]);
	registers[rt] = negativeNumber(registers[rt]);

	registers[rd] = registers[rs] + registers[rt];
	return 0;
}

/**
	The srl execution. Shifts bits to the right by the amount specified
	in shamt.
**/

int exec_srl(unsigned int rs, unsigned int rt, unsigned int rd, unsigned int shamt)
{
	registers[rd] = registers[rt] >> shamt;   //shift bits by amount in shamt
	return 0;
}

/**
	The sll execution. Shifts bits to the left by the amount specified
	in shamt.
**/

int exec_sll(unsigned int rs, unsigned int rt, unsigned int rd, unsigned int shamt)
{
	registers[rd] = registers[rt] << shamt;  //shift bits by amount in shamt
	return 0;
}

/**
	The addi execution. Checks if the immediate is negative then 
	stores the result of the register + immediate in the register.
**/

int exec_addi(unsigned int rs, unsigned int rt, int imm)
{
	imm = negativeNumber(imm);
	registers[rt] = registers[rs] + imm;   //set register to register value + immediate
	return 0;
}

/**
	The andi execution. Stores the result of a bitwise and 
	of the register and the immediate in rt.
**/

int exec_andi(unsigned int rs, unsigned int rt, int imm)
{
	registers[rt] = registers[rs] & imm;   //bitwise and
	return 0;
}

/**
	The beq execution. Checks if the values in the registers are equal
	and if they are then adds the immediate value to the program counter
**/

int exec_beq(unsigned int rs, unsigned int rt, int imm)
{
	if(registers[rs] == registers[rt])
	{
		imm = negativeNumber(imm);
		imm *=4;
		pc += imm;
	}
	
	return 0;
}

/**
	The bne execution. Checks if the values in the registers are NOT equal
	and if they are not, adds the immediate value to the program counter
**/

int exec_bne(unsigned int rs, unsigned int rt, int imm)
{
	if(registers[rs] != registers[rt])
	{
		imm = negativeNumber(imm);
		imm *= 4;
		pc += imm;
	}
	
	return 0;
}

const unsigned int opcodebyte[] = {0x00, 0x08, 0x0c, 0x04, 0x05}; 

opcode_functions execcalls[] = {exec_addi, exec_addi, exec_andi, exec_beq, exec_bne}; //used to call each of the executions of bytecode

const unsigned int functbyte[] = {0x20, 0x02, 0x00};	

funct_functions functcalls[] = {exec_add, exec_srl, exec_sll};		//for calling each of the executions of bytecode that have functs

//bytecodefunc

int exec_bytecode(){
        printf("EXECUTING PROGRAM ...\n");

        pc = ADDR_TEXT; //set program counter to the start of our program

        int maxopcodebytes = 5;

        int pos = 26;     //Check which position of the 32-bit bytecode we are reading

        int noCheck = 6;  //Number of bits to read

        int opcode = 0;

        int rs = 0; //where value is stored

        int rt = 0; //register 1

        int rd = 0; //register 2

        int shamt = 0;

        int funct = 0;

        int maxfunctcode = 3;

        int imm = 0;  //immediate value

        lastInstruction &= 0x004FFFF;

        unsigned int bytecode = text[(pc & 0x004FFFF)];

        while((pc & 0x004FFFF) < lastInstruction)  // While not reading the last instruction
        {
        	pos = 26;

        	noCheck = 6;

        	opcode = ((bytecode >> pos) & ((1 << noCheck)-1));

        	for(int j = 0; j < maxopcodebytes; j++)
        	{
        		if(bytecode == 0x00000000)  //If the bytecode is 0x00000000 (no operation)...
        		{
        			printf("Executing No Op...\n");
        			pc+= 4;
        			break;
        		}

		        if(opcode == opcodebyte[j]) //If the opcode exists in the opcode array
		        {
		        
		        	if(opcode == 0x00) //If its an R-type instruction
		        	{ //All R-type instructions in this example start with 0x00

		       			noCheck = 5;	//checking 5 bits for each register store

		       			pos = pos - 5;

		       			rs = ((bytecode >> pos) & ((1 << noCheck)-1));   //get rs from bytecode

		       			pos = pos - 5;

		       			rt = ((bytecode >> pos) & ((1 << noCheck)-1));   //get rt from bytecode

		       			pos = pos - 5;

		       			rd = ((bytecode >> pos) & ((1 << noCheck)-1));   //get rd from bytecode

		       			pos = pos - 5;

		        		shamt = ((bytecode >> pos) & ((1 << noCheck)-1));  

		        		noCheck = 6;

		        		pos = pos - 6;

		       			funct = ((bytecode >> pos) & ((1 << noCheck)-1));

		       			for(int x = 0; x < maxfunctcode; x++)
		       			{
		       				if(funct == functbyte[x])    //If the funct code matches one in the functbyte array...
		        			{
		        				printf("Executing... 0x%08x 0x%08x\n", pc, bytecode);
		        				pc += 4;
		        				functcalls[x](rs, rt, rd, shamt);        //Execute the function at the same position.
		        				break;
		        			}
		        		}
		        			
		        		}
			        	else //if an I-type instruction
			        	{
			        			noCheck = 5;

			        			pos = pos - 5;

			        			rs = ((bytecode >> pos) & ((1 << noCheck)-1));

			        			pos = pos - 5;

			        			rt = ((bytecode >> pos) & ((1 << noCheck)-1));

			        			noCheck = 16;

			        			pos = pos - 16;

			        			imm = ((bytecode >> pos) & ((1 << noCheck)-1));

			        		for(int x = 0; x < 5; x++)
			        		{
			        			if(opcode == opcodebyte[x])		 //If the opcode code matches one in the opcodebyte array...
			        			{
			        				printf("Executing... 0x%08x 0x%08x\n", pc, bytecode);	
			        				pc += 4;
			        				execcalls[x](rs, rt, imm);	 //Execute the function at the same position.
			        				break;
			        			}
			        		}
		        		}
	        		}
        		}

        	bytecode = text[((pc & 0x004FFFF)/4)];   //Load the text array at position (& is used to cancel bits 0x004)
        	registers[0] = 0;
        }
        

        print_registers(); // print out the state of registers at the end of execution

        printf("... DONE!\n");
        return(0);

}


int load_program(){
       int j=0;
       FILE *f;

       printf("LOADING PROGRAM ...\n");

       f = fopen ("prog.txt", "r");

       if (f==NULL) {
       		printf("ERROR: program not found\n");
       }

       while(fgets(&prog[prog_len][0], MAX_LINE_LEN, f) != NULL) {
               	prog_len++;
       }

       printf("PROGRAM:\n");
       for (j=0;j<prog_len;j++){
               	printf("%d: %s",j, &prog[j][0]);
       }

       printf("... DONE!\n");

       return(0);
}


int main(){
	if (load_program()<0) 	return(-1);        
	if (make_bytecode()<0) 	return(-1); 
	if (exec_bytecode()<0) 	return(-1); 
   	return(0);
}

